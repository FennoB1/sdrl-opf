import copy

import gym
import numpy as np
import torch
from drl.ddpg import *


class SummationTd3(Td3):
    def __init__(self, env, *args, **kwargs):
        super().__init__(env, *args, **kwargs)
        self.objectives = 6
        self.constraints = 2
        self.penalty_factor = kwargs['penalty_factor'] if 'penalty_factor' in kwargs else 1
        print("Penalty factor is:", self.penalty_factor)

    def reward2scalar(self, reward):
        if np.isscalar(reward):
            return reward
        obj = np.sum(reward[0:self.objectives])
        penalty = np.sum(
            reward[self.objectives:self.objectives + self.constraints])
        return obj + penalty * self.penalty_factor

    def remember(self, obs, action, reward, next_obs, done):
        super().remember(obs, action, self.reward2scalar(reward), next_obs, done)


class ReplacementTd3(Td3):
    def __init__(self, env, *args, **kwargs):
        super().__init__(env, *args, **kwargs)
        self.objectives = 6
        self.constraints = 2

    def reward2scalar(self, reward):
        if np.isscalar(reward):
            return reward
        obj = np.sum(reward[0:self.objectives])
        penalty = np.sum(
            reward[self.objectives:self.objectives + self.constraints])
        if penalty != 0.0:
            return 0.01 * penalty
        return 10 + 0.01 * obj

    def remember(self, obs, action, reward, next_obs, done):
        super().remember(obs, action, self.reward2scalar(reward), next_obs, done)


# Constrained Td3
# Uses two double Q estimators, one for the relaxed objective R_t - lmd * Rc_t
#   (R_t is the reward and Rc_t the constraint violation penalty)
#  and one for the constraint violation penalty Rc_t
# Both Q functions are learned with soft target update (is this necessary?)
class CTd3(Td3):
    def __init__(self, env, *args, **kwargs):
        self.objectives = 6
        self.constraints = 2
        self.lg_lambda = 0 if self.constraints == 1 else torch.zeros(
            self.constraints)
        self.lambda_step = 0.01
        self.max_lambda_step = 0.001
        self.max_violations = 0.0
        self.losses = []
        self.c_losses = []
        super().__init__(env, *args, **kwargs)

    def _init_networks(self, actor_fc_dims, actor_learning_rate,
                       critic_fc_dims, critic_learning_rate,
                       output_activation, **kwargs):
        self.actor = DDPGActorNet(
            self.n_obs, actor_fc_dims, self.n_act, actor_learning_rate,
            output_activation=output_activation, **kwargs)
        self.actor_target = copy.deepcopy(self.actor)

        self.critic = DDPGCriticNet(
            self.n_obs, self.n_act, critic_learning_rate, critic_fc_dims,
            n_rewards=1, **kwargs)
        self.critic_target = copy.deepcopy(self.critic)
        self.critic2 = DDPGCriticNet(
            self.n_obs, self.n_act, critic_learning_rate, critic_fc_dims,
            n_rewards=1, **kwargs)
        self.critic2_target = copy.deepcopy(self.critic2)

        self.c_critic = DDPGCriticNet(
            self.n_obs, self.n_act, critic_learning_rate, critic_fc_dims,
            n_rewards=self.constraints, **kwargs)
        self.c_critic2 = DDPGCriticNet(
            self.n_obs, self.n_act, critic_learning_rate, critic_fc_dims,
            n_rewards=self.constraints, **kwargs)
        self.c_critic_target = copy.deepcopy(self.c_critic)
        self.c_critic2_target = copy.deepcopy(self.c_critic2)

    def _learn(self, obss, acts, rewards, next_obss, dones):
        self._train_critic(obss, acts, rewards, next_obss, dones)
        if self.step % self.update_delay == 0:
            self._train_actor(obss, acts, rewards, next_obss, dones)
            # Update all (!) targets delayed
            self._soft_target_update(self.actor, self.actor_target, self.tau)
            self._soft_target_update(self.critic, self.critic_target, self.tau)
            self._soft_target_update(
                self.critic2, self.critic2_target, self.tau)
            self._soft_target_update(
                self.c_critic, self.c_critic_target, self.tau)
            self._soft_target_update(
                self.c_critic2, self.c_critic2_target, self.tau)
            # lambda update
            self._lambda_update(obss)

    def _lambda_update(self, obss):
        if self.step > 3500:
            acts = self.actor_target(obss)
            c_estim = torch.maximum(self.c_critic(
                obss, acts, ), self.c_critic2(obss, acts, ))
            for i in range(self.constraints):
                avg_c_value = torch.mean(c_estim[i])
                print("avg_c_value ", i, ": ", float(avg_c_value))
                lstep = self.lambda_step * \
                    float(avg_c_value - self.max_violations)
                self.lg_lambda[i] = max(
                    0.0,
                    self.lg_lambda[i] + np.clip(
                        lstep,
                        -self.max_lambda_step,
                        self.max_lambda_step
                    )
                )
            print("lambda: ", self.lg_lambda)

    def _train_critic(self, obss, acts, rewards, next_obss, dones):
        # rewards need to be split into obj and constraints
        o_rewards = torch.sum(rewards[:, 0:self.objectives], axis=1)
        c_rewards = rewards[:, -self.constraints:]

        l_rewards = 1.0 / (1.0 + sum(self.lg_lambda)) * (
            (o_rewards + torch.sum(self.lg_lambda * c_rewards, 1)))

        self.critic.optimizer.zero_grad()
        q_values = self.critic(obss, acts, )
        targets = self._compute_targets(next_obss, dones, l_rewards)
        critic_loss = self.critic.loss(targets, q_values)
        print("critic_loss: ", float(torch.sqrt(critic_loss)))
        self.losses.append(float(critic_loss))
        critic_loss.backward()
        if self.grad_clip is not None:
            torch.nn.utils.clip_grad_norm_(
                self.critic.parameters(), self.grad_clip)
        self.critic.optimizer.step()

        self.critic2.optimizer.zero_grad()
        q_values = self.critic2(obss, acts, )
        critic_loss = self.critic2.loss(targets, q_values)
        print("critic2_loss: ", float(torch.sqrt(critic_loss)))
        critic_loss.backward()
        if self.grad_clip is not None:
            torch.nn.utils.clip_grad_norm_(
                self.critic.parameters(), self.grad_clip)
        self.critic2.optimizer.step()

        self.c_critic.optimizer.zero_grad()
        self.c_critic2.optimizer.zero_grad()
        cq_values1 = self.c_critic(obss, acts, )
        cq_values2 = self.c_critic2(obss, acts, )
        c_targets = self._compute_c_targets(next_obss, dones, c_rewards)
        critic1_loss = self.c_critic.loss(c_targets, cq_values1)
        print("c_critic1_loss: ", float(torch.sqrt(critic1_loss)))
        self.c_losses.append(float(critic1_loss))
        critic1_loss.backward()
        critic2_loss = self.c_critic2.loss(c_targets, cq_values2)
        print("c_critic2_loss: ", float(torch.sqrt(critic2_loss)))
        critic2_loss.backward()
        if self.grad_clip is not None:
            torch.nn.utils.clip_grad_norm_(
                self.c_critic.parameters(), self.grad_clip)
        if self.grad_clip is not None:
            torch.nn.utils.clip_grad_norm_(
                self.c_critic2.parameters(), self.grad_clip)
        self.c_critic.optimizer.step()
        self.c_critic2.optimizer.step()

    @torch.no_grad()
    def _compute_targets(self, next_obss, dones, rewards):
        """ Add noise to target actions and use min q value from 2 critics. """
        next_acts = self.actor_target(next_obss)
        noise = (
            torch.randn_like(next_acts) * self.target_noise_std_dev
        ).clamp(-self.noise_clip, self.noise_clip)
        next_acts = (next_acts + noise).clamp(self.min_range, 1)

        target_values1 = self.critic_target(next_obss, next_acts)
        target_values2 = self.critic2_target(next_obss, next_acts)
        target_values = torch.minimum(target_values1, target_values2)
        target_values[dones == 1.0] = 0.0
        return rewards + (self.gamma * target_values)

    @torch.no_grad()
    def _compute_c_targets(self, next_obss, dones, rewards):
        """ Add noise to target actions and use min q value from 2 critics. """
        next_acts = self.actor_target(next_obss)
        noise = (
            torch.randn_like(next_acts) * self.target_noise_std_dev
        ).clamp(-self.noise_clip, self.noise_clip)
        next_acts = (next_acts + noise).clamp(self.min_range, 1)

        target_values1 = self.c_critic_target(next_obss, next_acts)
        target_values2 = self.c_critic2_target(next_obss, next_acts)
        target_values = torch.maximum(target_values1, target_values2)
        target_values[dones == 1.0] = 0.0
        return rewards + (self.gamma * target_values)
