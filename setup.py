""" Done with https://towardsdatascience.com/create-your-custom-python-package-that-you-can-pip-install-from-your-git-repository-f90465867893"""

from setuptools import setup, find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='sdrl-opf',
    version='0.0.1',
    author='Fenno Boomgaarden',
    author_email='fenno.boomgaarden@t-online.de',
    description='Improved constraint satisfaction SDRL algorithms for OPF',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(include=['sdrl-opf', 'sdrl-opf.*']),
    url='https://gitlab.com/FennoB1/sdrl-opf',
    license='MIT',
    install_requires=[
        'gym==0.21.0',
        'gym-notices==0.0.8',
        'matplotlib==3.4.0',
        'numba==0.56.4',
        'numpy==1.18.3',
        'pandapower==2.2.2',
        'pandas==1.3.5',
        'PettingZoo==1.14.0',
        'pybullet==3.2.5',
        'scipy==1.4.1',
        'simbench==1.2.0',
        'torch==2.0.0',
    ],
    dependency_links=[
        'https://gitlab.com/thomaswolgast/drl@d90270a2fc8e483831c92190869dc02eb182d300#egg=drl',
        'https://gitlab.com/thomaswolgast/mlopf@5bb8631b2d855c5b306cb7b1907b37d70a26f433#egg=mlopf',
    ],
)
